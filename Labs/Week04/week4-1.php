<?php
$lower = 2;
$upper = 5;

// for loop
echo "Using for loop:\n";
for ($i = $lower; $i <= $upper; $i++) {
    echo $i . "\n";
}

// while loop
echo "\nUsing while loop:\n";
$i = $lower;
while ($i <= $upper) {
    echo $i . "\n";
    $i++;
}

//  do-while loop
echo "\nUsing do-while loop:\n";
$i = $lower;
do {
    echo $i . "\n";
    $i++;
} while ($i <= $upper);
?>
