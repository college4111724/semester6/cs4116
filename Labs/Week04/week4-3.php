<?php
$lower = 1;
$upper = 10;

// for loop from upper to lower skip 3
echo "Using for loop:\n";
for ($i = $upper; $i >= $lower; $i -= 3) {
    echo $i . "\n";
}

// while loop from upper to lower skip 3
echo "\nUsing while loop:\n";
$i = $upper;
while ($i >= $lower) {
    echo $i . "\n";
    $i -= 3;
    
}

//  do-while from upper to lower skip 3
echo "\nUsing do-while loop:\n";
$i = $upper;
do {
    echo $i . "\n";
    $i -= 3;
} while ($i >= $lower);
?>
