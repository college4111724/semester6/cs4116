<?php
$key = [23,16,81,14,99];
$value = ["John", "Edward", "Aidan", "Sheraz", "Atif"] ;
$newArray = array_combine($value, $key);


echo "Iterating array and displaying matching key and values:\n";
for ($i = 0; $i < count($key); $i++) {
    echo "Key: " . $key[$i] . ", Value: " . $value[$i] . "\n";
}

echo "\n";
echo "\n";

echo "Finding the highest key value and outputting it:\n";
$maxFinder = $key[0]; 
for ($i = 1; $i < count($key); $i++) {
    if ($key[$i] > $maxFinder) {
        $maxFinder= $key[$i];
    }
}


echo "Highest Key: " . $maxFinder . "\n";

echo "/n";
echo "/n";

echo "This part is literally done earlier? Lol";


// Increment each value by 10
echo "Incrementing each value by 10:\n";
foreach ($newArray as $name => &$value) {
    $value += 10;
}

// Output the new array
foreach ($newArray as $name => $key) {
    echo "Name: $name, Key: $key\n";
}
?>