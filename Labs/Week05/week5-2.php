<?php
session_start();

if(isset($_POST['name']) && !empty($_POST['name'])) {
    $_SESSION['name'] = $_POST['name'];
}


if(isset($_POST['age']) && !empty($_POST['age'])) {
    $_SESSION['age'] = $_POST['age'];
    header("Location: week5-2.php"); 
    exit();
}


if(isset($_SESSION['name']) && isset($_SESSION['age'])) {
    $name = $_SESSION['name'];
    $age  = $_SESSION['age'];
    echo "<h1>Hello $name, you are $age years of age</h1>";
    exit();
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Week05</title>
</head>
<body>
    <h1>Week05</h1>

    <form action="week5-2.php" method="post">
        <?php 
        
        if (isset($_SESSION['name'])) {
            $name = $_SESSION['name'];
            echo "<p>Hello $name</p>";
            echo '<label for="age">How old are you?</label>
                  <input type="number" name="age" required><br>';
        } else {
            // If name is not set, prompt for name
            echo '<label for="name">Name:</label>
                  <input type="text" name="name" required><br>';
        }
        ?>

        <button type="submit">Submit</button>
    </form>
    
</body>
</html>
